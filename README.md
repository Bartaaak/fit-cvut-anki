# FIT ČVUT Anki kartičky

## Úvod
Do tohoto repozitáře budu postupně přidávat všechny svoje Anki kartičky a jejich aktualizace vytvořené k předmětům na FIT ČVUT.

## Předměty
### 2022/2023 ZS
 - [BI-AAG](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/master/BI-AAG)
 - [BI-AG1](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/master/BI-AG1)
 - [BI-APS](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/master/BI-APS)
 - [BI-MA2](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/master/BI-MA2)
 - [BI-UKB](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/master/BI-UKB)

### 2021/2022 LS
 - [BI-DBS](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/master/BI-DBS)
 - [BI-MA1](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/master/BI-MA1)
 - [BI-PA2](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/master/BI-PA2)
 - [BI-PSI](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/master/BI-PSI)
 - [BI-SAP](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/master/BI-SAP)

### 2021/2022 ZS
 - [BI-LA1](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/master/BI-LA1)
 - [BI-PA1](https://gitlab.com/xskoda/fit-cvut-anki/-/tree/master/BI-PA1)

