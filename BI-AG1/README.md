# BI-AG1

## Vynechaná témata
 - všechny důkazy (krom jednoho) věty o charakterizaci stromů ([3:2](https://courses.fit.cvut.cz/BI-AG1/media/lectures/bi-ag1-p3-handout.pdf))
 - všechna lemmata o binární haldě (krom těch o korektnosti algoritmů) ([4](https://courses.fit.cvut.cz/BI-AG1/media/lectures/bi-ag1-p4-handout.pdf))
 - všechna lemmata k binomiálním stromům ([5](https://courses.fit.cvut.cz/BI-AG1/media/lectures/bi-ag1-p5-handout.pdf))
 - důkaz tvrzení o výskytu binomiálního stromu řádu i v binomiální haldě ([5:19](https://courses.fit.cvut.cz/BI-AG1/media/lectures/bi-ag1-p5-handout.pdf))
